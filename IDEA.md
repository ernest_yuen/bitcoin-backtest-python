# Trading Ideas

1. 
2.
3. 
4. 
5. 

## Fair Price V.S. Price
### Correlation 
* ~ 0.05 
* Changes in Fair Price has no implication on the Price
* -> cannot trigger signal.

### Crossing of the two lines 
* Long Short

### Linear Regression
* Bet on the premium relationship
* If it exceeds threshold
* Then do the reverse

### Fair Price
* If N-day Standard Score of fair price above threshold 
* -> LONG BTC one day OR LONG til it crosses back the threshold
* Vice versa
* FAIL

### P/FV
* This ratio above N day Standard Score
* The current price is strong relative to the fair price
* LONG BTC
* Vice versa
