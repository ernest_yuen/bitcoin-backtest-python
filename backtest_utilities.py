import datetime
import logging
import time

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(message)s')

file_handler = logging.FileHandler('backtest_performance.log')
file_handler.setLevel(logging.INFO)
file_handler.setFormatter(formatter)

# stream_handler = logging.StreamHandler()
# stream_handler.setFormatter(formatter)
# logger.addHandler(stream_handler)
logger.addHandler(file_handler)


class PerformanceMatrices:

    time_factor_dict = {
        'daily': 252,
        'weekly': 52,
        'monthly': 12,
        'quarterly': 4
    }

    # transaction_cost: dict

    def __init__(self, pnl: pd.Series, starting_capital=100000,
                 risk_free_rate=0.0, resolution='daily', portfolio_approach=False,
                 benchmark=None, strategy_description=None):
        self._equity = starting_capital
        self._profit_and_loss = pnl
        self._cumulative_pnl_pd_series = np.cumsum(self._profit_and_loss) + 1
        self._cumulative_pnl_pd_series.fillna(0, inplace=True)
        self._resolution = resolution.lower()
        self._risk_free_rate = risk_free_rate

        logger.info('Backtest starts')

        if self.time_factor_dict.get(self._resolution) is None:
            logger.debug('[WARNING] The resolution parameter is incorrect.')

        self._time_factor = self.time_factor_dict[self._resolution]

        if portfolio_approach:
            self._drawdown_series = self._cumulative_pnl_pd_series/self._cumulative_pnl_pd_series.cummax() - 1
        else:
            self._drawdown_series = pd.Series(0.0, index=self._cumulative_pnl_pd_series.index)

            for i in range(1, len(self._cumulative_pnl_pd_series)):
                self._drawdown_series.iloc[i] = self._cumulative_pnl_pd_series.iloc[i] - max(self._cumulative_pnl_pd_series[:i+1])

        target = self._risk_free_rate
        self._downside_returns = self._profit_and_loss.loc[self._profit_and_loss < target]
        self._downside_stdev = self._downside_returns.std()
        self._sortino_ratio = (self._profit_and_loss.mean() - target) / self._downside_stdev

    def fetch_cumulative_pnl_series(self):
        return self._cumulative_pnl_pd_series

    def fetch_cumulative_return(self):
        return round(self._cumulative_pnl_pd_series[-1], 2)

    def fetch_annualized_return(self):
        return round(self._profit_and_loss.mean() * self._time_factor, 2)

    # TODO: Fill in the geometric return.
    def fetch_annualized_geometric_return(self):
        pass

    def fetch_annualized_volatility(self):
        return round(self._profit_and_loss.std() * np.sqrt(self._time_factor), 2)

    def fetch_annualized_sharpe_ratio(self):
        # TODO: two ways to calculate the sharpe ratio.
        # return (self._profit_and_loss.mean() - self._risk_free_rate) / self._profit_and_loss.std()
        return round(self.fetch_annualized_return() / self.fetch_annualized_volatility(), 2)

    def fetch_sortino_ratio(self):
        return round(self._sortino_ratio, 2)

    def fetch_drawdown_series(self):
        return self._drawdown_series

    def fetch_maximum_drawdown(self):
        try:
            result = round(min(self._drawdown_series), 2)
        except ValueError:
            result = 0.0
        return result

    def fetch_calmar_ratio(self):
        try:
            result = round(self.fetch_annualized_return() / abs(self.fetch_maximum_drawdown()), 2)
        except ZeroDivisionError:
            logger.exception(f'The maximum drawdown is zero.')
            result = 0.0
        return result

    def fetch_var(self, confidence_level=0.05):
        sorted_returns = self._profit_and_loss.sort_values(ascending=True, inplace=False)
        var = sorted_returns.quantile(confidence_level)
        return var

    def fetch_conditional_var(self, confidence_level=0.05):
        var = self.fetch_var(confidence_level=confidence_level)
        conditional_var = self._profit_and_loss[self._profit_and_loss.lt(var, axis=1)].mean()
        return conditional_var

    def fetch_win_rate(self):
        return round(1 - self._downside_returns.__len__() / self._profit_and_loss.__len__(), 2)

    def cumulative_pnl_plot(self):
        self._cumulative_pnl_pd_series.plot()
        plt.show()

    def drawdown_plot(self):
        self._drawdown_series.plot()
        plt.show()

    def performance_plot(self, save_fig=False, save_path=None):
        fig = plt.figure()
        ax1 = fig.add_subplot(211)
        plt.title(f'Sharpe Ratio: {self.fetch_annualized_sharpe_ratio()} | Annual Return: {self.fetch_annualized_return()}\n'
                  f'Maximum Drawdown: {self.fetch_maximum_drawdown()} | Calmar Ratio: {self.fetch_calmar_ratio()}')
        ax2 = fig.add_subplot(212)
        self._cumulative_pnl_pd_series.plot(color='blue', ax=ax1, figsize=(10, 10), label='Cumulative PnL', legend=True)
        self._drawdown_series.plot(color='red', kind='area', ax=ax2, figsize=(10, 10), label='Drawdown', legend=True)
        if save_fig:
            plt.savefig(save_path)
            logger.info('Result graph saved as {}'.format(save_path))
        else:
            plt.show()
