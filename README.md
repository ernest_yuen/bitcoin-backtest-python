# Bitcoin-Backtest-Python

## Relevant websites
* https://www.coinfairvalue.com/coins/bitcoin/
* https://www.coinfairvalue.com/reference/

### What is a fair value?

The concept of fair value might sound familiar to an investor. There are two kinds of assets; those whose fair value is intrinsic, and those whose fair value can be expressed as a function of intrinsic fair values.

The intrinsic fair value is the function that converts the ordinal preferences of an individual’s mind into a price. Therefore, the price at which a person buys something ordinary (eg. a banana) is always smaller or equal to the fair value that the individual assigns to the thing he is buying. When many people trade an ordinary good or service, we can refer to the current fair value of that good or service as the price of it. If we were to invest in that ordinary good or service (eg. bananas), then we would need to predict the future changes in the scale of preferences that exist in society with regard to the specific ordinary good or service. We can refer to this unit as; the fair value, predicted fair value, future fair value, or speculated fair value.

We can further conclude that the compounded fair value represents any fair value that is expressed as a function of intrinsic fair values, the existence of which can be justified under the rules of compounded assets. One would first assess the intrinsic fair values of the underlying ordinary assets, and then use the rational function to obtain a compounded fair value. Some examples of compounded assets with compounded fair values are stocks, bonds, derivatives, mortgages or currencies. All these derive their fair values from intrinsic fair values like cash flows, interest rates, ordinary goods and services, or even other compounded fair values (such as in the case of financial derivatives).

Any compounded fair value has a degree of uncertainty. The source of biggest uncertainty is usually the inclusion of future intrinsic fair values in the rational model. One of the best-known variables based upon the future, included in almost any fair value model is the interest rate.

One last thing to mention is that the cardinal value of something can only be expressed in terms of another. There is no such thing as an absolute price. Every price comprises of a pair of assets which consists of the priced asset, and the reference asset. For instance, one can say that a car is worth ’N’ units of a currency (eg. USD), however it is impossible to imply that the car is worth an absolute number. Everything has a relative value in economics because the value of everything is derived from the ordinal scale of preferences that apply to each and every individual. Therefore, when determining the fair value of a currency, one will find themselves always comparing the value of one currency to another.