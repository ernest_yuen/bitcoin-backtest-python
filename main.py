import pandas as pd
import matplotlib.pyplot as plt

from backtest_utilities import PerformanceMatrices

d = './dataset'
f = pd.read_csv(f'{d}/fv_bitcoin_usd.csv')
df = f.copy()
df = df[df['DateTime'] > '2017']
df.reset_index(inplace=True)
# df.index = df['DateTime']
df['DateTime'] = pd.to_datetime(df['DateTime'])
df['FV'] = df['FV (USD)'].pct_change()
df['P'] = df['P (USD)'].pct_change()
df['P'].corr(df['FV'])

lookback_period = 60
z_score = 2
df['ma'] = df['FV'].rolling(window=lookback_period).mean()
df['sd'] = df['FV'].rolling(window=lookback_period).std()
df['signal_long'] = (df['FV'] - df['ma']) / df['sd'] > z_score
df['signal_short'] = (df['FV'] - df['ma']) / df['sd'] < -z_score

t = pd.Series(0.0, index=df['DateTime'])

for i in range(len(df) - 1):
    if df['signal_long'].iloc[i]:
        t.iloc[i] = df['P'].iloc[i+1]
    elif df['signal_short'].iloc[i]:
        t.iloc[i] = -df['P'].iloc[i+1]
    else:
        continue

pf = PerformanceMatrices(pnl=t)
pf.performance_plot()

